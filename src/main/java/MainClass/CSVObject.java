package MainClass;

public class CSVObject {
    private String state_or_territory;
    private String travel_associated_cases;
    private String locally_acquired_cases;
    private String time;
    private CSVObject obj;

    CSVObject() {

    }

    CSVObject (CSVObject o) {
        state_or_territory = o.state_or_territory;
        travel_associated_cases = o.travel_associated_cases;
        locally_acquired_cases = o.locally_acquired_cases;
        time = o.time;
    }

    CSVObject (
            String s,
            String t,
            String l,
            String tr) {
        state_or_territory = s;
        travel_associated_cases = t;
        locally_acquired_cases = l;
        time = tr;
    }

    public CSVObject get() {
        return obj;
    }

    // Getter
    public String get_state_or_territory() {
        return state_or_territory;
    }

    // Setter
    public void set_state_or_territory(String newName) {
        this.state_or_territory = newName;
    }

    // Getter
    public String get_travel_associated_cases() {
        return travel_associated_cases;
    }

    // Setter
    public void set_travel_associated_cases(String newName) {
        this.travel_associated_cases = newName;
    }

    // Getter
    public String get_locally_acquired_cases() {
        return locally_acquired_cases;
    }

    // Setter
    public void set_locally_acquired_cases(String newName) {
        this.locally_acquired_cases = newName;
    }

    // Getter
    public String get_time() {
        return time;
    }

    // Setter
    public void set_time(String newName) {
        this.time = newName;
    }
}
